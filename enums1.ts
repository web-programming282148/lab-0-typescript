enum CadinaalDirections {
    North = "North",
    East = "East",
    South = "South",
    West = "West"
}

let currentDirection = CadinaalDirections.East;

console.log(currentDirection);
